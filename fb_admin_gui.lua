--[[
FishBus Gaming permissions system - Admin GUI

Shamelessly originating from ExplosiveGamings's mod.
permissions are based on ranks.
--]]

require 'event'
require 'mod-gui'

require 'fb_ranks'
require 'fb_util'
require 'fb_util_gui'
require 'fb_util_playertable'
require 'fb_automessage'

local frameName = 'admintools'
local frameCaption = 'Admin'

fbgui.createFrame(frameName,3,'Player List',frameCaption,"All admin fuctions are here")

fbgui.connectButton('btn_toolbar_automessage', function()
	autoMessage()
end)
fbgui.connectButton('tp_all', function(player,frame)
	for i,p in pairs(game.connected_players) do
		local pos = game.surfaces[player.surface.name].find_non_colliding_position("player", player.position, 32, 1)
		if p ~= player then
			p.teleport(pos)
		end
	end
end)
fbgui.connectButton('revive_dead_entitys_range', function(player,frame)
	if tonumber(frame.parent.range.text) then
		local range = tonumber(frame.parent.range.text)
		if range > 200 then
			player.print("Unable to revive a range greater than 200.")
			return
		end
		local region = {{player.position.x-range,player.position.y-range}, {player.position.x+range,player.position.y+range}}
		for key, entity in pairs(game.surfaces[1].find_entities_filtered({area=region, type = "entity-ghost"})) do
			entity.revive()
		end
	end
end)
fbgui.connectButton('add_dev_items', function(player,frame)
	player.insert{name="deconstruction-planner", count = 1}
	player.insert{name="blueprint-book", count = 1}
	player.insert{name="blueprint", count = 20}
end)
fbgui.connectButton('sendMessage', function(player,frame)
	local rank = remote.call('fb_ranks', 'stringToRank', frame.parent.message.rank.items[frame.parent.message.rank.selected_index])
	if rank then
		remote.call('fb_ranks', 'callRank', frame.parent.message.message.text,rank.name)
	end
end)
fbgui.connectButton('setRanks', function(player,frame)
	rank = remote.call('fb_ranks', 'stringToRank', frame.parent.rank_input.items[frame.parent.rank_input.selected_index])
	if rank then
		for _,playerName in pairs(global.selected[player.index]) do
			p=game.players[playerName]
			p1_rank = remote.call("fb_ranks", 'getRank', player)
			p2_rank = remote.call("fb_ranks", 'getRank', p)
			if p1_rank.power < p2_rank.power and rank.power > p1_rank.power then
				remote.call('fb_ranks', 'giveRank', p,rank,player)
				clearSelection(player)
				drawPlayerTable(player, frame.parent.parent, false, true, {})
			else
				player.print('You can not edit '..p.name.."'s rank there rank is too high (or the rank you have slected is above you)")
			end
		end
	end
end)
fbgui.connectButton('clearSelection',function(player,frame)
	clearSelection(player)
	drawPlayerTable(player, frame.parent.parent, false, true, {})
end)

fbgui.createTab(frameName, 'Commands', 'Random useful commands',
	function(player, frame)
		fbgui.createButton(frame,'btn_toolbar_automessage','Auto Message','Send the auto message to all online players')
		fbgui.createButton(frame,'add_dev_items','Get Blueprints','Get all the blueprints')
		fbgui.createButton(frame,'revive_dead_entitys_range','Revive Entitys','Brings all dead machines back to life in player range')
		frame.add{type='textfield',name='range',text='Range'}
		frame.add{type='flow',name='message'}
		frame.message.add{type='textfield',name='message',text='Enter message'}
		frame.message.add{type='drop-down',name='rank'}
		for _,rank in pairs(global.ranks) do
			if rank.power >= remote.call("fb_ranks", 'getRank', player).power
				then frame.message.rank.add_item(rank.name)
			end
		end
		frame.message.rank.selected_index = 1
		fbgui.createButton(frame,'sendMessage','Send Message','Send a message to all ranks higher than the slected')
		fbgui.createButton(frame,'tp_all','TP All Here','Brings all players to you')
	end)
fbgui.createTab(frameName,'Edit Ranks', 'Edit the ranks of players below you',
	function(player,frame)
		clearSelection(player)
		frame.add{name='filterTable',type='table',colspan=2}
		frame.filterTable.add{name='name_label',type='label',caption='Name'}
		frame.filterTable.add{name='sel_label',type='label',caption='Selected?'}
		frame.filterTable.add{name='name_input',type='textfield'}
		frame.filterTable.add{name='sel_input',type='textfield'}
		frame.add{type='flow',name='rank',direction='horizontal'}
		frame.rank.add{name='rank_label',type='label',caption='Rank'}
		frame.rank.add{name='rank_input',type='drop-down'}
		for _,rank in pairs(global.ranks) do
			if rank.power > remote.call("fb_ranks", 'getRank', player).power then
				frame.rank.rank_input.add_item(rank.name)
			end
		end
		frame.rank.rank_input.selected_index = 1
		fbgui.createButton(frame.rank,'setRanks','Set Ranks','Sets the rank of all selected players')
		fbgui.createButton(frame.rank,'clearSelection','Clear Selection','Clears all currently selected players')
		drawPlayerTable(player, frame, false, true, {'lower'})
	end)
fbgui.createTab(frameName, 'Player List', 'Send player message to all players',
	function(player, frame)
		frame.add{name='filterTable',type='table',colspan=2}
		frame.filterTable.add{name='name_label',type='label',caption='Name'}
		frame.filterTable.add{name='hours_label',type='label',caption='Online Time (minutes)'}
		frame.filterTable.add{name='name_input',type='textfield'}
		frame.filterTable.add{name='hours_input',type='textfield'}
		drawPlayerTable(player, frame, true,false, {'online'})
	end)

-- Additional button functionality
fbgui.connectButton('goto', function(player, frame)
	local p = game.players[frame.parent.name]
	player.teleport(game.surfaces[p.surface.name].find_non_colliding_position("player", p.position, 32, 1))
end)
fbgui.connectButton('bring', function(player, frame)
	local p = game.players[frame.parent.name]
	p.teleport(game.surfaces[player.surface.name].find_non_colliding_position("player", player.position, 32, 1))
end)
fbgui.connectButton('jail', function(player, frame)
	local p=game.players[frame.parent.name]
	if p.permission_group.name ~= 'Jail' then
		remote.call('fb_ranks', 'giveRank', p, 'Jail', player)
	else
		remote.call('fb_ranks', 'revertRank', p, player)
	end
end)
fbgui.connectButton('kill', function(player, frame)
	local p = game.players[frame.parent.name]
	if p.character then
		p.character.die()
	end
end)
fbgui.connectButton('revert', function(player, frame)
	local p = game.players[frame.parent.name]
	remote.call('fb_ranks', 'revertRank', p, player)
end)



Event.register(defines.events.on_player_joined_game, function(event)
	local player = game.players[event.player_index]
	local myFrame = fbgui.getFrame(frameName)[1]
	
	-- create button of specific name.  That gets it to link up to the action
	-- automatically defined by createFrame(...)
	if remote.call('fb_ranks', 'getRank', player).power <= myFrame.require then
		fbgui.createToolbarButton(player, "btn_"..frameName, myFrame.caption, myFrame.tooltip, myFrame.sprite)
	end
end)

Event.register("custom.fb_rankchange", function(event)
	local player = event.player
	local myFrame = fbgui.getFrame(frameName)[1]
   	if remote.call('fb_ranks', 'getRank', player).power <= myFrame.require then
		fbgui.createToolbarButton(player, "btn_"..frameName, myFrame.caption, myFrame.tooltip, myFrame.sprite)
	else
		fbgui.removeToolbarButton(player, "btn_"..frameName)
		-- close the frame too, else they can still click stuff!
		player.gui.center.clear()
	end
end)
