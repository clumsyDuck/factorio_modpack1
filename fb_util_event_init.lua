--[[
	Copyright 2017 Kovus <kovus@soulless.wtf>
	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
	1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	FishBus Gaming - Initialization Event for softmods (not actual game init)
	
	Create an on-join initialization point for soft mods.
	
	Files which initialize data should dispatch a new event:
	Event.register("custom.softmod_initialize", function(event) ... end)
	
--]]

require 'event'

Event.register(defines.events.on_player_joined_game, function(event)
	if not global.mods_initialized then
		global.mods_initialized = true
		Event.dispatch({name = "custom.softmod_initialize", tick = game.tick})
	end
end)
Event.register(defines.events.on_player_created, function(event)
	if not global.mods_initialized then
		global.mods_initialized = true
		Event.dispatch({name = "custom.softmod_initialize", tick = game.tick})
	end
end)
