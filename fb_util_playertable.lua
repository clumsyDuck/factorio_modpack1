--[[
FishBus Gaming permissions system - Player Table rendering information

Shamelessly originating from ExplosiveGamings's mod.
permissions are based on ranks.
--]]

function drawPlayerTable(player, frame, commands, select, filters)
	--setup the table
	if frame.playerTable then
		frame.playerTable.destroy()
	end
	pTable = frame.add{name='playerTable', type="table", colspan=5}
	pTable.style.minimal_width = 500
	pTable.style.maximal_width = 500
	pTable.style.horizontal_spacing = 10
	pTable.add{name="id", type="label", caption="Id"}
	pTable.add{name="Pname", type="label", caption="Name"}
	if commands == false and select == false then
		pTable.add{name="status", type="label", caption="Status"}
	end
	pTable.add{name="online_time", type="label", caption="Online Time"}
	pTable.add{name="rank", type="label", caption="Rank"}
	if commands then
		pTable.add{name="commands", type="label", caption="Commands"}
	end
	if select then
		pTable.add{name="select_label", type="label", caption="Selection"}
	end
	--filter checking
	for i, p in pairs(game.players) do
		local addPlayer = true
		for _,filter in pairs(filters) do
			if filter == 'admin' then
				if p.admin == false then
					addPlayer = false
					break
				end
			elseif filter == 'online' then
				if p.connected == false then
					addPlayer = false
					break
				end
			elseif filter == 'offline' then
				if p.connected == true then
					addPlayer = false
					break
				end
			elseif filter == 'lower' then
				local p1rank = remote.call("fb_ranks", 'getRank', p)
				local p2rank = remote.call("fb_ranks", 'getRank', player)
				if p1rank.power <= p2rank.power then
					addPlayer = false
					break
				end
			elseif filter == 'selected' then
				local Break = nil
				for _,name in pairs(global.selected[player.index]) do
					if name == p.name then
						Break = true
						break
					end
				end
				if not Break then
					addPlayer = false
					break
				end
			elseif type(filter)=='number' then
				if filter > ticktominutes(p.online_time) then
					addPlayer = false
					break
				end
			elseif type(filter)=='string' then
				if p.name:lower():find(filter:lower()) == nil then
					addPlayer = false 
					break 
				end
			end
		end
		--addes the player to the list
		if addPlayer == true and player.name ~= p.name then
			if pTable[p.name] == nil then
				local p1rank = remote.call("fb_ranks", 'getRank', p)
				local p2rank = remote.call("fb_ranks", 'getRank', player)
				pTable.add{name=i .. "id", type="label", caption=i}
				pTable.add{name=p.name..'_name', type="label", caption=p.name}
				--status
				if not commands and not select then
					if p.connected == true then
						pTable.add{name=p.name .. "Status", type="label", caption="ONLINE"}
					else
						pTable.add{name=p.name .. "Status", type="label", caption="OFFLINE"}
					end
				end
				--time and rank
				pTable.add{name=p.name .. "Online_Time", type="label", caption=(ticktohour(p.online_time)..'H '..(ticktominutes(p.online_time)-60*ticktohour(p.online_time))..'M')}
				pTable.add{
					name=p.name .. "Rank", 
					type="label", 
					caption=p1rank.shortHand
				}
				--commands
				if commands then
					pTable.add{name=p.name, type="flow"}
					fbgui.createButton(pTable[p.name],'goto','Tp','Goto to the players location')
					fbgui.createButton(pTable[p.name],'bring','Br','Bring player player to your location')
					if p1rank.power > p2rank.power then
						fbgui.createButton(pTable[p.name],'jail','Ja','Jail/Unjail player')
						fbgui.createButton(pTable[p.name],'revert','Re','Set A players rank to their forma one')
						fbgui.createButton(pTable[p.name],'kill','Ki','Kill this player')
					end
					--player selection
				elseif select then
					pTable.add{name=p.name, type="flow"}
					local state = false
					for _,name in pairs(global.selected[player.index]) do if name == p.name then state = true break end end
					pTable[p.name].add{name='select', type="checkbox",state=state}
				end
			end
		end
	end
end
