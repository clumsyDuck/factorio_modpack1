--[[
Hello there script explorer! 

With this you can add a "Fish Market" to your World
You can earn fish by killing alot of biters or by mining wood, ores, rocks.
To spawn the market, do "/c market()" in your chat ingame as the games host.
It will spawn a few tiles north of the current position where your character is.

---MewMew---
--]]
--[[

Updates (Kovus):
- Dependency created upon fb_score.lua through the use of the global.score when
killing biters.
- Added interface for the market, so that it can be spawned through other
means, and avoid killing your achievements.
- Adjusted the market() function to become spawnMarketOn(player) so that the
market can be spawned on the player calling for it.

--]]

require 'event'

function spawnMarketOn(player)
  local radius = 10
  local surface = game.surfaces[1]
  -- clear trees and landfill in start area
  local start_area = {left_top = {-20, -20}, right_bottom = {20, 20}}
  for _, e in pairs(surface.find_entities_filtered{area=start_area, type="tree"}) do
    --e.destroy()
  end
  for i = -radius, radius, 1 do
    for j = -radius, radius, 1 do
      if (surface.get_tile(i,j).collides_with("water-tile")) then
        --surface.set_tiles{{name = "grass", position = {i,j}}}
      end
    end
  end
  
  local market_location = {x = player.position.x, y = player.position.y}
  market_location.y = market_location.y - 4
  
  -- create water around market
  local waterTiles = {}
  for i = -4, 4 do
    for j = -4, 4 do
        --table.insert(waterTiles, {name = "water-green", position={market_location.x + i, market_location.y + j}})
    end
  end
  surface.set_tiles(waterTiles)
  local market = surface.create_entity{name="market", position=market_location, force=force}
  market.destructible = false

  market.add_market_item{price={{"raw-fish", 1}}, offer={type="give-item", item="rail", count=2}}
  market.add_market_item{price={{"raw-fish", 2}}, offer={type="give-item", item="rail-signal"}}
  market.add_market_item{price={{"raw-fish", 2}}, offer={type="give-item", item="rail-chain-signal"}}
  market.add_market_item{price={{"raw-fish", 15}}, offer={type="give-item", item="train-stop"}}
  market.add_market_item{price={{"raw-fish", 75}}, offer={type="give-item", item="locomotive"}}
  market.add_market_item{price={{"raw-fish", 30}}, offer={type="give-item", item="cargo-wagon"}}
  market.add_market_item{price={{"raw-fish", 1}}, offer={type="give-item", item="red-wire", count=2}}
  market.add_market_item{price={{"raw-fish", 1}}, offer={type="give-item", item="green-wire", count=2}}
  market.add_market_item{price={{"raw-fish", 3}}, offer={type="give-item", item="decider-combinator"}}
  market.add_market_item{price={{"raw-fish", 3}}, offer={type="give-item", item="arithmetic-combinator"}}
  market.add_market_item{price={{"raw-fish", 3}}, offer={type="give-item", item="constant-combinator"}}
  market.add_market_item{price={{"raw-fish", 7}}, offer={type="give-item", item="programmable-speaker"}}   
  market.add_market_item{price={{"raw-fish", 3}}, offer={type="give-item", item="piercing-rounds-magazine"}}
  market.add_market_item{price={{"raw-fish", 2}}, offer={type="give-item", item="grenade"}}
  market.add_market_item{price={{"raw-fish", 1}}, offer={type="give-item", item="land-mine"}}
  market.add_market_item{price={{"raw-fish", 1}}, offer={type="give-item", item="solid-fuel"}}
  market.add_market_item{price={{"raw-fish", 125}}, offer={type="give-item", item="rocket-launcher"}}
  market.add_market_item{price={{"raw-fish", 15}}, offer={type="give-item", item="rocket"}}   
  market.add_market_item{price={{"raw-fish", 20}}, offer={type="give-item", item="explosive-rocket"}}  
  market.add_market_item{price={{"raw-fish", 2500}}, offer={type="give-item", item="atomic-bomb"}} 
  market.add_market_item{price={{"raw-fish", 1000}}, offer={type="give-item", item="belt-immunity-equipment"}} 
end

function removeMarketsNear(player, range)
	local surface = game.surfaces[1]
	local start_area = {
		left_top = {player.position.x - range, player.position.y - range},
		right_bottom = {player.position.x + range, player.position.y + range}
	}
	local count = 0
	for _, e in pairs(surface.find_entities_filtered{area=start_area, type="market"}) do
		e.destroy()
		count = count + 1
	end
	return count
end

function market()
	spawnMarketOn(game.players[1])
end
	
local function create_market_init_button(event)
	local player = game.players[1]
	
	if player.gui.top.poll == nil then
		local button = player.gui.top.add { name = "poll", type = "sprite-button", sprite = "item/programmable-speaker" }
		button.style.font = "default-bold"
		button.style.minimal_height = 38
		button.style.minimal_width = 38
		button.style.top_padding = 2
		button.style.left_padding = 4
		button.style.right_padding = 4
		button.style.bottom_padding = 2
	end
end
--FISH MESSAGES TO BE DELETED IN FUTURE -SPLICER
local fish_market_message = {}

local fish_market_bonus_message = {""}

local total_fish_market_messages = #fish_market_message
local total_fish_market_bonus_messages = #fish_market_bonus_message

local function mod_initialize()
	if not global.fish_market_fish_caught then
		global.fish_market_fish_caught = {}
	end
end
mod_initialize()

local function fish_earned(event, amount)

	local player = game.players[event.player_index]
	player.insert { name = "raw-fish", count = amount }
	
	if global.fish_market_fish_caught[event.player_index] then
		global.fish_market_fish_caught[event.player_index] = global.fish_market_fish_caught[event.player_index] + 1
	else
		global.fish_market_fish_caught[event.player_index] = 1
	end
	
	if global.fish_market_fish_caught[event.player_index] <= total_fish_market_messages then
		local x = global.fish_market_fish_caught[event.player_index]
		player.print(fish_market_message[x])
	end		
	
	local x = global.fish_market_fish_caught[event.player_index] % 7
	if x == 0 then
		local z = math.random(1,total_fish_market_bonus_messages)
		player.print(fish_market_bonus_message[z])
	end			

end

local function preplayer_mined_item(event)

--	game.print(event.entity.name)
--	game.print(event.entity.type)

	if event.entity.type == "resource" then
		local x = math.random(1,2)		
		if x == 1 then		
			fish_earned(event, 1)
		end		
	end
	
	-- This doesn't make any sense, and you can't insert 0 of an item.
	--if event.entity.name == "fish" then			
	--		fish_earned(event, 0)	
	--end
	
	if event.entity.name == "stone-rock" then			
			fish_earned(event, 10)	
	end
	
	if event.entity.name == "huge-rock" then			
			fish_earned(event, 25)	
	end
	
	if event.entity.name == "big-rock" then			
			fish_earned(event, 15)	
	end

	if event.entity.type == "tree" then
		local x = math.random(1,4)
		if x == 1 then		
			fish_earned(event, 4)
		end
	end
end

local function fish_drop_entity_died(event)
	
	if event.entity.force.name == "enemy" then
--		global.score.biters_killed = global.score.biters_killed + 1
--		game.print(global.score.biters_killed)
		if global.score.biters_killed % 30 == 0 then
			local surface = event.entity.surface
			local x = math.random(1,3)
			surface.spill_item_stack(event.entity.position, { name = "raw-fish", count = x }, 1)
		end
	end		
end

remote.add_interface("fish_market", {
	spawnMarketOn = spawnMarketOn,
	removeMarketsNear = removeMarketsNear,
})

Event.register(defines.events.on_preplayer_mined_item, preplayer_mined_item)
Event.register(defines.events.on_entity_died, fish_drop_entity_died)

Event.register("custom.softmod_initialize", function(event)
	mod_initialize()
end)