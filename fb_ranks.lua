--[[
FishBus Gaming permissions system - Rank management.

Shamelessly originating from ExplosiveGamings's mod.
permissions are based on ranks.
--]]
--[[
To consider: it might be better "security" to turn the command set from
"Allow all except these things" to "Deny everything except these things."
It depends on how big the command set really is.
--]]

require 'event'

local defaults = {
	--for disallow add to the list the end part of the input action
	--example: defines.input_action.drop_item -> 'drop_item'
	--http://lua-api.factorio.com/latest/defines.html#defines.input_action
	ranks={
		{name='Owner',shortHand='Owner',tag='[Owner]',power=0,colour={r=170,g=0,b=0},disallow={}},
		{name='Community Manager',shortHand='Com Mngr',tag='[Com Mngr]',power=1,colour={r=150,g=68,b=161},disallow={}},
		{name='Developer',shortHand='Dev',tag='[Dev]',power=1,colour={r=179,g=125,b=46},disallow={}},
		{name='Admin',shortHand='Admin',tag='[Admin]',power=2,colour={r=233,g=63,b=233},disallow={'set_allow_commands','edit_permission_group','delete_permission_group','add_permission_group'}},
		{name='Mod',shortHand='Mod',tag='[Mod]',power=3,colour={r=0,g=170,b=0},disallow={'set_allow_commands','server_command','edit_permission_group','delete_permission_group','add_permission_group'}},
		{name='Donator',shortHand='P2W',tag='[P2W]',power=4,colour={r=233,g=63,b=233},disallow={'set_allow_commands','server_command','edit_permission_group','delete_permission_group','add_permission_group'}},
		{name='Member',shortHand='Mem',tag='[Member]',power=5,colour={r=24,g=172,b=188},disallow={'set_allow_commands','server_command','edit_permission_group','delete_permission_group','add_permission_group'}},
		{name='Regular',shortHand='Reg',tag='[Regular]',power=5,colour={r=24,g=172,b=188},disallow={'set_auto_launch_rocket','change_programmable_speaker_alert_parameters','reset_assembling_machine','drop_item','set_allow_commands','server_command','edit_permission_group','delete_permission_group','add_permission_group'}},
		{name='Guest',shortHand='Guest',tag='[Guest]',power=6,colour={r=255,g=159,b=27},disallow={'build_terrain','remove_cables','launch_rocket','cancel_research','set_auto_launch_rocket','change_programmable_speaker_alert_parameters','reset_assembling_machine','drop_item','set_allow_commands','server_command','edit_permission_group','delete_permission_group','add_permission_group'}},
		{name='Jail',shortHand='Jail',tag='[Jail]',power=7,colour={r=50,g=50,b=50},disallow={'open_character_gui','begin_mining','start_walking','player_leave_game','build_terrain','remove_cables','launch_rocket','cancel_research','set_auto_launch_rocket','change_programmable_speaker_alert_parameters','reset_assembling_machine','drop_item','set_allow_commands','server_command','edit_permission_group','delete_permission_group','add_permission_group'}}
	},
	autoRanks={
		Owner={'Splicer9'},
		['Community Manager']={},
		Developer={'kovus', 'themostinternet'},
		Admin={},
		Mod={'dogigy', 'darkdwight7'},
		Donator={},
		Member={},
		Regular={},
		Guest={},
		Jail={}
	},
	selected={},
	oldRanks={}
}
local timeForRegular = 180

function fb_getRank(player)
	if player then
		for _,rank in pairs(global.ranks) do
			if player.permission_group == game.permissions.get_group(rank.name) then return rank end
		end
		return fb_stringToRank('Guest')
	end
end

function fb_stringToRank(string)
	if type(string) == 'string' then
		local Foundranks={}
		for _,rank in pairs(global.ranks) do
			if rank.name:lower() == string:lower() then return rank end
			if rank.name:lower():find(string:lower()) then table.insert(Foundranks,rank) end
		end
		if #Foundranks == 1 then return Foundranks[1] end
	end
end

function fb_callRank(msg, rank, inv)
	local rank = fb_stringToRank(rank) or fb_stringToRank('Mod') -- default mod or higher
	local inv = inv or false
	for _, player in pairs(game.players) do
		rankPower = fb_getRank(player).power
		if inv then
			if rankPower >= rank.power then
				player.print(('[Everyone]: '..msg))
			end
		else
			if rankPower <= rank.power then
				if rank.shortHand ~= '' then
					player.print(('['..(rank.shortHand)..']: '..msg))
				else
					player.print(('[Everyone]: '..msg))
				end
			end
		end
	end
end

function fb_giveRank(player,rank,byPlayer)
	local byPlayer = byPlayer or 'system'
	local rank = fb_stringToRank(rank) or rank or fb_stringToRank('Guest')
	local oldRank = fb_getRank(player)
	local message = 'demoted'
	if rank.power <= oldRank.power then
		message = 'promoted'
	end
	if byPlayer.name then
		fb_callRank(player.name..' was '..message..' to '..rank.name..' by '..byPlayer.name,'Guest')
	else
		fb_callRank(player.name..' was '..message..' to '..rank.name..' by <system>','Guest')
	end
	player.permission_group = game.permissions.get_group(rank.name)
	--drawToolbar(player)
	--drawPlayerList()
	global.oldRanks[player.index]=oldRank.name
	
	Event.dispatch({name = 'custom.fb_rankchange', tick = game.tick, player = player})
end

function fb_revertRank(player,byPlayer)
	local rank = fb_stringToRank(global.oldRanks[player.index])
	fb_giveRank(player,rank,byPlayer)
end

function fb_autoRank(player)
	local currentRank = fb_getRank(player)
	local playerAutoRank = nil
	local oldRank = fb_getRank(player)
	for rank,players in pairs(global.autoRanks) do
		local Break = false
		for _,p in pairs(players) do
			if player.name:lower() == p:lower() then
				playerAutoRank = fb_stringToRank(rank)
				Break = true
				break
			end
		end
		if Break then
			break
		end
	end
	if playerAutoRank == nil then
		if ticktominutes(player.online_time) >= timeForRegular then
			playerAutoRank = fb_stringToRank('Regular')
		else
			playerAutoRank = fb_stringToRank('Guest')
		end
	end
	if currentRank.name ~='Jail' and currentRank.power > playerAutoRank.power or currentRank.name == 'Guest' then
		if playerAutoRank.name == 'Guest' then
			player.permission_group=game.permissions.get_group('Guest')
		else
			fb_giveRank(player,playerAutoRank)
		end
	end
	if fb_getRank(player).power <= 3 and not player.admin then
		fb_callRank(player.name..' needs to be promoted.')
	end
	if oldRank.name ~= fb_getRank(player).name then
		global.oldRanks[player.index]=oldRank.name
	end
end

remote.add_interface("fb_ranks", {
	getRank = fb_getRank,
	stringToRank = fb_stringToRank,
	callRank = fb_callRank,
	giveRank = fb_giveRank,
	revertRank = fb_revertRank,
	autoRank = fb_autoRank,
})

Event.register(defines.events.on_player_joined_game, function(event)
	--runs when the first player joins to make the permission groups
	if global.ranks == nil then
		for name,value in pairs(defaults) do
			global[name] = value
		end
		for _,rank in pairs(global.ranks) do
			game.permissions.create_group(rank.name)
			for _,toRemove in pairs(rank.disallow) do
				game.permissions.get_group(rank.name).set_allows_action(defines.input_action[toRemove],false)
			end
		end
	end
	--Standard start up
	local player = game.players[event.player_index]
	fb_autoRank(player)
	player.print({"", "Welcome"})
	Event.dispatch({name = 'custom.fb_rankchange', tick = game.tick, player = player})
	
	game.print(player.name .. " has joined with rank " .. remote.call("fb_ranks", 'getRank', player).shortHand)
	--drawPlayerList()
	--drawToolbar(player)
	--if not player.admin and ticktominutes(player.online_time) < 1 then
	--	drawFrame(player,'Readme','Rules')
	--end
end)

