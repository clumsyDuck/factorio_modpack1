--[[
FishBus Gaming permissions system - Anti-Griefing measures

Shamelessly originating from ExplosiveGamings's mod.
permissions are based on ranks.
--]]

require 'fb_util'
require 'fb_ranks'

local timeForRegular = 180

Event.register(defines.events.on_marked_for_deconstruction, function(event)
	local eplayer = game.players[event.player_index]
	if remote.call('fb_ranks', 'getRank', eplayer).power > 5 then
		if event.entity.type ~= "tree" and event.entity.type ~= "simple-entity" then
			event.entity.cancel_deconstruction("player")
			eplayer.print("You are not allowed to do this yet, play for player bit longer. Try again in about: " .. math.floor((timeForRegular - ticktominutes(eplayer.online_time))) .. " minutes")
			remote.call('fb_ranks', 'callRank', eplayer.name .. " tried to deconstruct something")
		end
	--elseif event.entity.type == "tree" or event.entity.type == "simple-entity" and remote.call('fb_ranks', 'getRank', eplayer).power < 5 then
	--	event.entity.destroy()
	end
end)
