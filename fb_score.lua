--[[
FishBus Gaming - Game score.

Originated from a mod called 'score'
Implements a biter kill count + rocket launched score.

Updates:
Also includes items launched & received from launches.
-Kovus

--]]

require 'event'
require 'mod-gui'

require 'fb_util_gui'
require 'fb_util_event_init'

local function mod_initialize()
	if not global.score then
		global.score = {
			rockets_launched = 0,
			biters_killed = 0,
			items_launched = {},
			items_received = {},
		}
	end
end
mod_initialize()
Event.register("custom.softmod_initialize", function(event)
	mod_initialize()
end)

local function drawScore(player)
	local flow = mod_gui.get_frame_flow(player)
	if flow.score == nil then
		local frame = flow.add{
			type = "frame",
			name = "score",
			direction = "vertical",
			style = mod_gui.frame_style
		}

		-- original used a horizonal layout.  I would rather it be more
		-- vertical since we're now using the 
		local score_table = frame.add { type = "table", colspan = 1, name = "score_table" }
		
		local label = score_table.add { type = "label", caption = "", name = "label_biters_killed" }
		label.style.font = "default-bold"
		label.style.font_color = { r=0.98, g=0.11, b=0.11 }
		label.style.top_padding = 2
		label.style.left_padding = 4
		label.style.right_padding = 4
		
		label = score_table.add { type = "label", caption = "", name = "label_rockets_launched" }
		label.style.font = "default-bold"
		label.style.font_color = { r=0.98, g=0.66, b=0.22 }
		label.style.top_padding = 2
		label.style.left_padding = 4
		label.style.right_padding = 4

		label = score_table.add { type = "label", caption = "Items launched in rockets:", name = "label_objects_launched" }
		label.style.font = "default-bold"
		label.style.font_color = { r=1, g=1, b=1 }
		label.style.top_padding = 2
		label.style.left_padding = 4
		label.style.right_padding = 4
		
		local itemtable = score_table.add { type = 'table', colspan = 2, name = 'sent_table' }
		itemtable.style.left_padding = 15
		
		label = score_table.add { type = "label", caption = "Items received from space:", name = "label_objects_received" }
		label.style.font = "default-bold"
		label.style.font_color = { r=1, g=1, b=1 }
		label.style.top_padding = 2
		label.style.left_padding = 4
		label.style.right_padding = 4
		
		itemtable = score_table.add { type = 'table', colspan = 2, name = 'recv_table' }
		itemtable.style.left_padding = 10
		
		refresh_score()
	end
end

local function drawScoreButton(player)
	local frame = mod_gui.get_button_flow(player)
	if not frame.btn_toolbar_playerList then
		fbgui.createButton(frame, "btn_score", "Score", "Show/Hide the rockets launched & biters killed score.", 'item/rocket-silo')
	end
end

function refresh_score()
	for _, player in pairs(game.connected_players) do
		local frame = mod_gui.get_frame_flow(player).score
		
		if (frame) then -- this should always be true....
			frame.score_table.label_rockets_launched.caption = "Rockets Launched: " .. global.score.rockets_launched
			frame.score_table.label_biters_killed.caption = "Biters Slaughtered: " .. global.score.biters_killed
			for name, count in pairs(global.score.items_launched) do
				if not frame.score_table.sent_table["sent_count_" .. name] then
					local itemtable = frame.score_table.sent_table
					itemtable.add { type = 'label', name = "sent_label_" .. name, caption = name}
					itemtable.add { type = 'label', name = "sent_count_" .. name, caption = count}
				else
					frame.score_table.sent_table["sent_count_" .. name].caption = count
				end
			end
			for name, count in pairs(global.score.items_received) do
				if not frame.score_table.recv_table["recv_count_" .. name] then
					local itemtable = frame.score_table.recv_table
					itemtable.add { type = 'label', name = "recv_label_" .. name, caption = name}
					itemtable.add { type = 'label', name = "recv_count_" .. name, caption = count}
				else
					frame.score_table.recv_table["recv_count_" .. name].caption = count
				end
			end
		end
	end
end

local function rocket_launched(event)
	global.score.rockets_launched = global.score.rockets_launched + 1
	game.print ("A rocket has been launched!")
	-- so, these are valid.  This is how to get the inventory sent on the rocket
	-- and how to get the return value.  it's sort of uninteresting to do this
	-- as a mod, given the built-in rocket silo stats 'mod'
	--game.print(serpent.block(event.rocket.get_inventory(defines.inventory.chest).get_contents()))
	--game.print(event.rocket.get_inventory(defines.inventory.chest)[1].name .. ": " .. event.rocket.get_inventory(defines.inventory.chest)[1].count)
	--game.print(serpent.block(event.rocket_silo.get_output_inventory().get_contents()))
	--game.print(event.rocket_silo.get_output_inventory()[1].name .. ": " .. event.rocket_silo.get_output_inventory()[1].count)
	local launch_inventory = event.rocket.get_inventory(defines.inventory.chest)
	if launch_inventory and launch_inventory.get_item_count() > 0 then
		--game.print("Rocket inventory? " .. launch_inventory[1].name)
		local item = launch_inventory[1]
		local count = item.count
		if global.score.items_launched[item.name] then
			count = count + global.score.items_launched[item.name]
		end
		global.score.items_launched[item.name] = count
	end
	
	local received_inventory = event.rocket_silo.get_output_inventory()
	if received_inventory and received_inventory.get_item_count() > 0 then
		--game.print("Receive inventory? " .. received_inventory[1].name)
		local item = received_inventory[1]
		local count = item.count
		if global.score.items_received[item.name] then
			count = count + global.score.items_received[item.name]
		end
		global.score.items_received[item.name] = count
	end
	
	refresh_score()
end


Event.register(defines.events.on_player_joined_game, function(event)
	local player = game.players[event.player_index]
	drawScore(player)
	drawScoreButton(player)
	fbgui.toggleVisible(mod_gui.get_frame_flow(player).score)
end)

Event.register(defines.events.on_gui_click, function(event)
	if not (event and event.element and event.element.valid) then return end
	
	if(event.element.type == 'sprite-button' or event.element.type == 'button') then
		if(event.element.name == 'btn_score') then
			local player = game.players[event.player_index]
			fbgui.toggleVisible(mod_gui.get_frame_flow(player).score)
		end
	end
end)

local function biter_kill_counter(event)	
	if event.entity.force.name == "enemy" then
		global.score.biters_killed = global.score.biters_killed + 1
	end		
end

Event.register(defines.events.on_entity_died, biter_kill_counter)
Event.register(defines.events.on_entity_died, refresh_score)
--Event.register(defines.events.on_gui_click, on_gui_click)
--Event.register(defines.events.on_player_joined_game, create_score_gui)
Event.register(defines.events.on_rocket_launched, rocket_launched)
