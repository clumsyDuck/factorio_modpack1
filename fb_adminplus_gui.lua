--[[
FishBus Gaming permissions system - Elevated Admin GUI

Shamelessly originating from ExplosiveGamings's mod.
permissions are based on ranks.
--]]

-- There may be events in here dependent on the regular 'Admin' gui.

require 'event'
require 'mod-gui'

require 'fb_ranks'
require 'fb_util'
require 'fb_util_gui'

local frameName = 'adminpptools'
local frameCaption = 'Admin+'
local forceModifiers = {
	"manual_mining_speed_modifier",
	"manual_crafting_speed_modifier",
	"character_running_speed_modifier",
	"worker_robots_speed_modifier",
	"worker_robots_storage_bonus",
	"character_build_distance_bonus",
	"character_item_drop_distance_bonus",
	"character_reach_distance_bonus",
	"character_resource_reach_distance_bonus",
	"character_item_pickup_distance_bonus",
	"character_loot_pickup_distance_bonus"
}

fbgui.createFrame(frameName, 2, 'Modifiers', frameCaption, "Because we are better")

fbgui.connectButton('remove_biters', function(player,frame)
	for key, entity in pairs(game.surfaces[1].find_entities_filtered({force='enemy'})) do
		entity.destroy()
	end
end)
fbgui.connectButton('toggle_cheat', function(player,frame)
	player.cheat_mode = not player.cheat_mode
end)
fbgui.connectButton('revive_dead_entitys', function(player,frame)
	for key, entity in pairs(game.surfaces[1].find_entities_filtered({type = "entity-ghost"})) do
		entity.revive()
	end
end)
fbgui.connectButton("btn_Modifier_apply", function(player,frame)
	for i, modifier in pairs(forceModifiers) do
		local number = tonumber(( frame.parent.parent.modifierTable[modifier .. "_input"].text):match("[%d.]*[%d+]"))
		if number ~= nil then
			if number == player.force[modifier] then
				player.print(modifier .. " did not change.")
			elseif number < 0 then
				player.print(modifier .. " needs to be >= 0.")
			elseif number >= 50 then
				player.print(modifier .. " must be below 50.")
			elseif number >= 0 and number < 50 and number ~= player.force[modifier] then
				player.force[modifier] = number
				player.print(modifier .. " changed to number: " .. tostring(number) .. ".")
			else
				player.print(modifier .. " contained an non-parseable number.")
			end
		end
	end
end)
fbgui.connectButton("btn_fish_market", function(player, frame)
	if remote.interfaces['fish_market'] and remote.interfaces['fish_market']['spawnMarketOn'] then
		remote.call('fish_market', 'spawnMarketOn', player)
		player.print("Fish Market spawned.")
	else
		player.print("Fish Market interface does not appear to be defined.")
	end
end)
fbgui.connectButton("btn_remove_fish_market", function(player, frame)
	if remote.interfaces['fish_market'] and remote.interfaces['fish_market']['removeMarketsNear'] then
		local count = remote.call('fish_market', 'removeMarketsNear', player, 10)
		if count > 0 then
			player.print(count .. " Fish Markets near you removed.")
		else
			player.print("No Fish Markets found near you.")
		end
	else
		player.print("Fish Market interface does not appear to be defined.")
	end
end)

fbgui.createTab(frameName, 'Commands', 'Random useful commands', function(player, frame)
	fbgui.createButton(frame,'btn_toolbar_automessage','Auto Message','Send the auto message to all online players')
	fbgui.createButton(frame,'btn_fish_market', 'Fish Market', 'Place a Fish Market right above your current location')
	fbgui.createButton(frame,'btn_remove_fish_market', 'Remove Nearby Markets', 'Removes Fish Markets near your current location')
	fbgui.createButton(frame,'add_dev_items','Get Blueprints','Get all the blueprints')
	fbgui.createButton(frame,'revive_dead_entitys','Revive All Entitys','Brings all dead machines back to life')
	fbgui.createButton(frame,'revive_dead_entitys_range','Revive Entitys','Brings all dead machines back to life in player range')
	frame.add{type='textfield',name='range',text='Range'}
	fbgui.createButton(frame,'remove_biters','Kill Biters','Removes all biters in map')
	fbgui.createButton(frame,'tp_all','TP All Here','Brings all players to you')
	fbgui.createButton(frame,'toggle_cheat','Toggle Cheat Mode','Toggle your cheat mode')
end)

fbgui.createTab(frameName, 'Modifiers', 'Edit in game modifiers', function(player,frame)
	frame.add{type = "flow", name= "flowNavigation",direction = "horizontal"}
	frame.add{name="modifierTable", type="table", colspan=3}
	frame.modifierTable.add{name="Mname", type="label", caption="name"}
	frame.modifierTable.add{name="input", type="label", caption="input"}
	frame.modifierTable.add{name="current", type="label", caption="current"}
	for i, modifier in pairs(forceModifiers) do
		frame.modifierTable.add{name=modifier, type="label", caption=modifier}
		frame.modifierTable.add{name=modifier .. "_input", type="textfield", caption="inputTextField"}
		frame.modifierTable.add{name=modifier .. "_current", type="label", caption=tostring(player.force[modifier])}
	end
	fbgui.createButton(frame.flowNavigation,"btn_Modifier_apply","Apply","Apply the new values to the game")
end)

Event.register(defines.events.on_player_joined_game, function(event)
	local player = game.players[event.player_index]
	local myFrame = fbgui.getFrame(frameName)[1]
	
	-- create button of specific name.  That gets it to link up to the action
	-- automatically defined by createFrame(...)
	if remote.call('fb_ranks', 'getRank', player).power <= myFrame.require then
		fbgui.createToolbarButton(player, "btn_"..frameName, myFrame.caption, myFrame.tooltip, myFrame.sprite)
	end
end)

Event.register("custom.fb_rankchange", function(event)
	local player = event.player
	local myFrame = fbgui.getFrame(frameName)[1]
   	if remote.call('fb_ranks', 'getRank', player).power <= myFrame.require then
		fbgui.createToolbarButton(player, "btn_"..frameName, myFrame.caption, myFrame.tooltip, myFrame.sprite)
	else
		fbgui.removeToolbarButton(player, "btn_"..frameName)
		-- close the frame too, else they can still click stuff!
		player.gui.center.clear()
	end
end)
