-- A part of band.lua
-- Feel free to edit.

return {

["to_print"] = {
 -- "%name now in the [%band] group.",
	"%name has joined the [%band] party.",
	"%name is now supporting the [%band] party."
},

["roles"] = {
	["Soldier"] = {
		"item/tank",
		"item/submachine-gun",	
		"item/combat-shotgun",
		"item/rocket-launcher",
		"item/grenade",
		"item/atomic-bomb",
	},
		
	["Mining"] = {
		"item/steel-axe",
		"item/iron-axe",
	},
	
	["Smelting"] = {
		"item/steel-furnace",
		"item/electric-furnace",
		"item/stone-furnace",
	},
	
	["Production"] = {
		"item/assembling-machine-1",
		"item/assembling-machine-2",
		"item/assembling-machine-3",
	},
	
	["Science"] = {
		"item/science-pack-1",
		"item/science-pack-2",
		"item/science-pack-3",
		"item/military-science-pack",
		"item/production-science-pack",
		"item/high-tech-science-pack",
		"item/space-science-pack",
	},

	["Circuits"] = {
		"item/green-wire",
		"item/red-wire",
		"item/arithmetic-combinator",
		"item/decider-combinator",
		"item/constant-combinator",
	},
	
	["Trains"] = {
		"item/cargo-wagon",
		"item/fluid-wagon",
		"item/locomotive",
		"item/rail-signal",
		"item/rail",
	},

	["Oil"] = {
		"item/pipe",
		"item/oil-refinery",
		"item/chemical-plant",
		"item/pumpjack",
		"fluid/crude-oil",
	},
	
	["Power"] = {
		"item/steam-turbine",
		"item/nuclear-reactor",
		"item/steam-engine",
		"item/solar-panel",
		"item/accumulator",
	},

	["Border Patrol"] = {
		"item/gun-turret",
		"item/stone-wall",
		"item/laser-turret",
		"item/land-mine",
	},

	["Quality Control"] = {
		"item/deconstruction-planner",
		"item/express-transport-belt",
		"item/productivity-module-3",
		"item/roboport",
	}
}, -- end of roles

}