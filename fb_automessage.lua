--[[
FishBus Gaming permissions system - Periodic Automessage

Shamelessly originating from ExplosiveGamings's mod.
permissions are based on ranks.
--]]

require 'fb_util'
require 'fb_ranks'

function autoMessage()
	if not game.connected_players then
		return
	end
	local lrank = 'Regular'
	local hrank = 'Mod'
	remote.call("fb_ranks", 'callRank', 'There are '..#game.connected_players..' players online', hrank, true)
	remote.call("fb_ranks", 'callRank', 'This map has been on for '..ticktohour(game.tick)..' Hours and '..(ticktominutes(game.tick)-60*ticktohour(game.tick))..' Minutes', hrank, true)
	remote.call("fb_ranks", 'callRank', 'Please join us on:', lrank, true)
	remote.call("fb_ranks", 'callRank', 'Discord: https://discord.gg/vdwxNUR', lrank, true)
	remote.call("fb_ranks", 'callRank', 'Website(incomplete): http://fish-bus.com', lrank, true)
	--remote.call("fb_ranks", 'callRank', 'Steam: http://steamcommunity.com/groups/tntexplosivegaming', lrank, true)
	remote.call("fb_ranks", 'callRank', 'To see these links again goto: Readme > Server Info', lrank, true)
	for _,player in pairs(game.connected_players) do
		remote.call('fb_ranks', 'autoRank', player)
	end
end

Event.register(defines.events.on_tick, function(event)
	if (game.tick/(3600*game.speed)) % 15 == 1 then
		autoMessage()
	end
end)
